package Globl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import static java.lang.System.out;

public class Globals {

    public static final String HOST = "localhost";
    public static final int PORT = 8888;
    public static final int BUFFER_SIZE = 128;
    public static final String SEPARATOR = "\n";
    public static final boolean DEBUG = false;
    public static final int FIELD_SIZE_X = 128;
    public static final int FIELD_SIZE_Y = 128;

    public static String readInput(SocketChannel channel, ByteBuffer buffer) throws IOException {

        buffer.clear();
        channel.read(buffer);
        buffer.flip();
        byte[] data = new byte[buffer.limit()];
        buffer.get(data);
        String msg = new String(data).trim();

        if(DEBUG) out.format("<-- RECV: %s%n", msg);

        return msg;
    }

    public static int writeMsg(SocketChannel channel, ByteBuffer buffer, String msg) throws IOException{

        buffer.clear();
        buffer.put((msg + SEPARATOR).getBytes());
        buffer.flip();
        int n = channel.write(buffer);

        if(DEBUG) out.format("--> SEND: %s%n", msg);

        return n;
    }
}

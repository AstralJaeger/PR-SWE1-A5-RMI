package Globl;

public class Player{

    private int posX;

    private int posY;

    private String name;

    public Player(int posX, int posY, String name){

        this.posX = posX;
        this.posY = posY;
        this.name = name;
    }

    public int getPosX() {

        return posX;
    }

    public void setPosX(int posX) {

        this.posX = posX;
    }

    public int getPosY() {

        return posY;
    }

    public void setPosY(int posY) {

        this.posY = posY;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null){

            return false;
        }

        if(!(obj instanceof Player)){

            return false;
        }

        Player other = (Player)obj;

        return other.name.equals(this.name) && other.posX == this.posX && other.posY == this.posY;

    }

    @Override
    public String toString(){

        return "[Player " + name + " x:" + posX + " y:" + posY + "]";
    }
}

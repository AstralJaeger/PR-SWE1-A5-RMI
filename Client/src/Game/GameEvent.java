package Game;

import java.util.EventObject;

public class GameEvent extends EventObject {

    private final JGameModel source;

    GameEvent(JGameModel source){

        super(source);
        this.source = source;
    }

    @Override
    public JGameModel getSource() {

        return source;
    }
}

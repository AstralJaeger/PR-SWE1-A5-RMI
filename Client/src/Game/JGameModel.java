package Game;

import Globl.Direction;
import Globl.Player;
import javafx.application.Application;

import javax.swing.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static Globl.Globals.*;
import static java.lang.System.in;
import static java.lang.System.out;

public class JGameModel {

    private List<GameListener> listeners;
    private List<Player> players;
    private String name;

    private ByteBuffer buffer;
    private SocketChannel channel;

    private Thread inputThread;
    private volatile boolean terminate = false;
    private volatile boolean login = true;

    JGameModel(String name){

        buffer = ByteBuffer.allocate(BUFFER_SIZE);

        listeners = new ArrayList<>();
        players = new ArrayList<>();

        this.name = name;

        try {

            channel = SocketChannel.open();
            channel.connect(new InetSocketAddress(HOST, PORT));

            inputThread = new Thread(new InputRunnable());
            inputThread.start();

        }catch (IOException e){

            JOptionPane.showMessageDialog(null, e.getMessage(), "Error loggin in", JOptionPane.INFORMATION_MESSAGE);
            System.exit(1);
        }
    }

    String getName(){

        return name;
    }

    List<Player> getPlayers(){

        return this.players;
    }

    void addGameListener(GameListener listener){

        listeners.add(listener);
    }

    @SuppressWarnings("unused")
    void removeGameListener(GameListener listener) {

        listeners.remove(listener);
    }

    @SuppressWarnings("WeakerAccess")
    void fireGameListener(){

        GameEvent event = new GameEvent(this);

        for(GameListener listener:listeners){

            listener.gameEvent(event);
        }
    }

    void move(Direction dir){

        try {

            writeMsg(channel, buffer, "MOVE " + dir.toString());
        }catch (IOException e){

            out.println(e.getMessage());
            e.printStackTrace(out);
        }
    }

    void logout(){

        try {

            writeMsg(channel, buffer, "LOGOUT");
        }catch (IOException e){
            // Dont care about exception at logout
        }

        terminate = true;
    }

    class InputRunnable implements Runnable{

        private String input = "";
        private ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);

        @Override
        public void run() {
            while (!terminate){

                try {

                    if(login){

                        login = false;
                        out.format("Logging in%n");
                        writeMsg(channel, buffer, "LOGIN " + name);
                    }

                    input = readInput(channel, buffer);

                    if(input.startsWith("MOVED")){

                        moved(input);
                    }else if(input.startsWith("REFUSED")){

                        refused(input);
                    } else if(input.startsWith("DISCONNECTED")) {

                        disconnected(input);
                    }
                    else{

                        out.println("Unknown command: " + input);
                        input = "";
                    }

                }catch (IOException e){

                    // ignore
                }
            }
        }

        private void moved(String input){

            String[] cmds = input.split(";");
            for(String cmd :cmds) {

                String[] params = cmd.replace("\n", "").split(" ");
                out.println("MOVED params: " + Arrays.toString(params));

                String name = params[1];
                int posX = Integer.parseInt(params[2]);
                int posY = Integer.parseInt(params[3]);

                if (!playerKnown(name)) {

                    players.add(new Player(posX, posY, params[1]));
                } else {

                    Player p = getByName(name);

                    p.setPosX(posX);
                    p.setPosY(posY);
                }
            }

            fireGameListener();
        }

        private void refused(String input){

            String msg = input.substring(input.indexOf(' '), input.length());
            JOptionPane.showMessageDialog(null, msg, "Login error", JOptionPane.WARNING_MESSAGE);
            GameClient.exit();
        }

        private void disconnected(String input){

            String[] params = input.replace(";", "").split(" ");
            out.println("Params: " + Arrays.toString(params));
            String name = params[1];

            Player removee = getByName(name);
            players.remove(removee);
            fireGameListener();
        }

        private boolean playerKnown(String name){

            for(Player p: players){

                if(p.getName().equals(name)){

                    return true;
                }
            }

            return false;
        }

        private Player getByName(String name){

            for (Player p: players) {

                if(p.getName().equals(name)){

                    return p;
                }
            }

            return null;
        }
    }
}

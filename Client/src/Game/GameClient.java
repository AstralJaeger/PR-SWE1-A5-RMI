package Game;

import Globl.Direction;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static java.lang.System.out;

public class GameClient {

    public static void main(String[] args) {

       SwingUtilities.invokeLater(()->{
            frame = createFrame();
            frame.setLocation(200,200);
            frame.setVisible(true);
        });
    }

    private static JFrame frame;
    private static JButton northBtn;
    private static JButton westBtn;
    private static JButton eastBtn;
    private static JButton southBtn;

    private static JGameModel model;

    private static JGameField field;

    private static JFrame createFrame(){

        JFrame window = new JFrame("Multiplayer game");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setSize(480, 640);
        window.getContentPane().setLayout(new BorderLayout());

        window.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {

                out.println("Frame closing");
                frame_closingEvent(e);
                super.windowClosing(e);
            }
        });

        // southPanel
        JPanel southPnl = new JPanel();
        southPnl.setLayout(new BorderLayout());
        window.getContentPane().add(southPnl, BorderLayout.SOUTH);

        southPnl.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {

                out.println("key pressed");
                southPnl_KeyPressedEvent(e);
                super.keyPressed(e);
            }
        });

        // North Button
        northBtn = new JButton("N");
        northBtn.addActionListener(GameClient::northBtn_downEvent);
        southPnl.add(northBtn, BorderLayout.NORTH);

        // West Button
        westBtn = new JButton("W");
        westBtn.addActionListener(GameClient::westBtn_downEvent);
        southPnl.add(westBtn, BorderLayout.WEST);

        // East Button
        eastBtn = new JButton("E");
        eastBtn.addActionListener(GameClient::eastBtn_downEvent);
        southPnl.add(eastBtn, BorderLayout.EAST);

        // South Button
        southBtn = new JButton("S");
        southBtn.addActionListener(GameClient::southBtn_downEvent);
        southPnl.add(southBtn, BorderLayout.SOUTH);

        // Game field
        String name;
        do {

            name = JOptionPane.showInputDialog(window, "Enter username: ").toUpperCase().trim();
        }while (name.length() <= 1 || name.contains(" "));

        window.setTitle("Game: " + name);
        model = new JGameModel(name);
        window.getContentPane().add(new JGameField(model), BorderLayout.CENTER);

        return window;
    }

    private static void frame_closingEvent(WindowEvent e){

        // close things here
        out.println("Frame closing");
        model.logout();
    }

    private static void southPnl_KeyPressedEvent(KeyEvent e){

        out.println("Key pressed");
    }

    private static void northBtn_downEvent(ActionEvent event){

        out.println("North button pressed");
        model.move(Direction.NORTH);
    }

    private static void eastBtn_downEvent(ActionEvent event){

        out.println("East button pressed");
        model.move(Direction.EAST);
    }

    private static void westBtn_downEvent(ActionEvent event){

        out.println("West button pressed");
        model.move(Direction.WEST);
    }

    private static void southBtn_downEvent(ActionEvent event){

        out.println("South button pressed");
        model.move(Direction.SOUTH);
    }

    public static void exit(){

        frame_closingEvent(null);
    }

}

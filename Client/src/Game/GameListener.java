package Game;

import java.util.EventListener;

public interface GameListener extends EventListener {

    void gameEvent(GameEvent event);
}

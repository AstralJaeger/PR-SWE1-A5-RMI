package Game;

import Globl.Player;
import javax.swing.*;
import java.awt.*;

import static Globl.Globals.*;

public class JGameField extends JComponent {

    private final JGameModel model;

    private final int MARGIN = 10;
    private final int PLAYER_SIZE = 8;
    private final int NAME_OFFSET = 5;
    private final int FONT_SIZE = 10;

    JGameField(JGameModel model){

        this.model = model;
        GameListener modelListener = event -> repaint();
        model.addGameListener(modelListener);
    }

    @Override
    protected void paintComponent(Graphics g){

        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D)g.create();
        g2d.setBackground(Color.WHITE);
        g2d.clearRect(0,0,getWidth(),getHeight());

        g2d.setPaint(Color.BLACK);
        g2d.drawLine(MARGIN, MARGIN, getWidth() - MARGIN, MARGIN);
        g2d.drawLine(MARGIN, MARGIN, MARGIN, getHeight()-MARGIN);
        g2d.drawLine(getWidth()-MARGIN, MARGIN, getWidth()-MARGIN, getHeight() - MARGIN);
        g2d.drawLine(MARGIN, getHeight()-MARGIN, getWidth()-MARGIN, getHeight()-MARGIN);

        if(DEBUG) {

            g2d.setPaint(Color.LIGHT_GRAY);
            float vStep = ((getWidth() - 2 * MARGIN) / (FIELD_SIZE_X + 0.0f));
            float hStep = ((getHeight() - 2 * MARGIN) / (FIELD_SIZE_Y + 0.0f));

            for (int i = 1; i < FIELD_SIZE_X; i++) {

                for (int j = 1; j < FIELD_SIZE_Y; j++) {

                    int px = Math.round(MARGIN + i * vStep);
                    int py = Math.round(MARGIN + j * hStep);

                    g2d.drawLine(px, py, px, py);
                    g2d.drawLine(px, py, px, py);
                }
            }
        }

        for(Player p : model.getPlayers()){

            if(p.getName().equals(model.getName())){

                g2d.setPaint(Color.RED);
            }else {

                g2d.setPaint(Color.GRAY);
            }

            drawPlayer(g2d, p);
        }
    }

    private void drawPlayer(Graphics2D g2d, Player p){

        int pX = MARGIN + Math.round((p.getPosX() * (getWidth() - 2 * MARGIN))/(FIELD_SIZE_X + 0.0F));
        int pY = MARGIN + Math.round((p.getPosY() * (getHeight() - 2 * MARGIN))/(FIELD_SIZE_Y + 0.0F));

        g2d.fillOval(pX-PLAYER_SIZE/2, pY-PLAYER_SIZE/2, PLAYER_SIZE, PLAYER_SIZE);
        g2d.setFont(new Font(g2d.getFont().getName(), Font.PLAIN, FONT_SIZE));
        g2d.drawString(p.getName(), pX + NAME_OFFSET, pY);
    }

}

package Game;

import Globl.Globals;
import Globl.Player;
import inout.In;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

import static Globl.Globals.*;
import static java.lang.System.out;

public class GameServer {

    public static void main(String[] args) throws IOException {

        out.println("Server starting");
        GameServer server = new GameServer();
        server.start();
    }

    private volatile boolean terminate = false;

    private Thread selectorThread;

    private Selector selector;

    private ByteBuffer buffer;

    private HashMap<SocketChannel, String> clients;

    private List<Player> players;

    private void start() throws IOException{

        buffer = ByteBuffer.allocate(Globals.BUFFER_SIZE);

        clients = new HashMap<>();
        players = new ArrayList<>();

        selector = Selector.open();

        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.bind(new InetSocketAddress(Globals.PORT));
        serverChannel.configureBlocking(false);
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);

        selectorThread = new Thread(new SelectorRunnable());
        selectorThread.start();

        out.println("Server started");

        // Terminate server
        out.println("Terminate with ANY CHAR + ENTER");

        In.readChar();
        terminate();

        try {

            selectorThread.join();
        }catch (InterruptedException e){

            e.printStackTrace(out);
        }

        out.println("Terminated Server");
    }

    private void terminate(){

        out.println("Terminating...");
        terminate = true;
    }

    private Player getByName(String name){

        for (Player p: players) {

            if(p.getName().equals(name)){

                return p;
            }
        }

        return null;
    }

    private Player movePlayer(Player p, String dir){

        int curX = p.getPosX();
        int curY = p.getPosY();

        switch (dir){

            case "NORTH":

                if(curY - 1 > 0){

                    p.setPosY(curY - 1);
                    out.format("Moved %s from Y%d to Y%d%n", p.getName(), curY, p.getPosY());
                }
                break;
            case "EAST":

                if(curX + 1 < FIELD_SIZE_X){

                    p.setPosX(curX + 1);
                    out.format("Moved %s from X%d to X%d%n", p.getName(), curX, p.getPosX());
                }
                break;
            case "WEST":

                if(curX - 1 > 0){

                    p.setPosX(curX - 1);
                    out.format("Moved %s from X%d to X%d%n", p.getName(), curX, p.getPosX());
                }
                break;
            case "SOUTH":

                if(curY + 1 < FIELD_SIZE_Y){

                    p.setPosY(curY + 1);
                    out.format("Moved %s from Y%d to Y%d%n", p.getName(), curY, p.getPosY());
                }
                break;
            default:

                out.format("%s is not a direction%n", dir);
                break;
        }

        return p;
    }

    class SelectorRunnable implements Runnable{

        @Override
        public void run(){

            while (!terminate){

                try {

                    @SuppressWarnings("unused")
                    int n = selector.select(2000);

                    Set<SelectionKey> keys = selector.selectedKeys();
                    Iterator<SelectionKey> keyIt = keys.iterator();

                    while (keyIt.hasNext()){

                        SelectionKey key = keyIt.next();

                        if(key.isAcceptable()){

                            ServerSocketChannel serverSocket = (ServerSocketChannel) key.channel();
                            SocketChannel clientChannel = serverSocket.accept();
                            clientChannel.configureBlocking(false);

                            SelectionKey selKey = clientChannel.register(selector, SelectionKey.OP_READ);

                        }else if(key.isReadable()){

                            SocketChannel channel = (SocketChannel)key.channel();
                            String msg = readInput(channel, buffer);

                            if(msg.startsWith("LOGIN")){

                                login(channel, msg);
                            } else if(msg.startsWith("MOVE")){

                                move(channel, msg);
                            } else if(msg.startsWith("LOGOUT")) {

                                logout(channel, msg);
                            } else{

                                out.println("Recv unknown command: " + msg);
                            }
                        }

                        keyIt.remove();
                    }

                }catch (Exception e){

                    out.println("Exception msg: " + e.getMessage());
                    e.printStackTrace(out);
                }
            }
        }

        private void login(SocketChannel channel, String msg) throws IOException{

            String name = msg.substring(msg.indexOf(' ') + 1);
            out.println("LOGIN request from: " + name);

            if(!clients.containsValue(name)){

                clients.put(channel, name);
                Random rnd = new Random();
                Player freshman = new Player(rnd.nextInt(FIELD_SIZE_X), rnd.nextInt(FIELD_SIZE_Y), name);
                players.add(freshman);

                // send position of the new player to every player
                for (SocketChannel chnl: clients.keySet()) {

                    writeMsg(chnl, buffer, "MOVED " + freshman.getName() + " " + freshman.getPosX() + " " + freshman.getPosY() + ";");
                }

                // Send position of every player to the new player
                for(Player p : players){

                    if(p.getName().equals(freshman.getName())){

                        continue;
                    }

                    writeMsg(channel, buffer, "MOVED " + p.getName() + " " + p.getPosX() + " " + p.getPosY() + ";");
                }
            }
            else {

                writeMsg(channel, buffer, "REFUSED User already exists;");
            }


        }

        private void move(SocketChannel channel, String msg) throws IOException{

            // Get playername that requests move
            String name = clients.get(channel);
            String dir = msg.substring(msg.indexOf(32) + 1, msg.length());

            out.format("Playermovenemt: '%s' -> '%s'%n", name, dir);

            // get player
            Player player = getByName(name);
            if(player != null){

                // move player
                movePlayer(player, dir);

                // notify clients about movement
                for(SocketChannel s : clients.keySet()){

                    writeMsg(s, buffer, "MOVED " + player.getName() + " "
                            + player.getPosX() + " "
                            + player.getPosY());
                }
            }
        }

        private void logout(SocketChannel channel, String msg) throws IOException{

            String name = clients.get(channel); // Get playername
            out.println("Logout req: " + name);

            clients.remove(channel);            // remove channel form map
            players.remove(getByName(name));    // remove player form list
            channel.close();                    // close channel

            // notify other clients about logout
            for(SocketChannel s : clients.keySet()){

                writeMsg(s, buffer, "DISCONNECTED " + name);
            }
        }
    }
}
